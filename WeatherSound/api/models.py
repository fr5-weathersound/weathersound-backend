from django.db import models


class Country(models.Model):
    code = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=50, null=True, default="")

    def __str__(self) -> str:
        return f"{self.name} ({self.code})"


class City(models.Model):
    geoname_id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=50,  null=True, default="")
    inhabitant = models.IntegerField(null=True)
    longitude = models.FloatField(null=True, default=0)
    latitude = models.FloatField(null=True, default=0)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.name} ({self.country})"
