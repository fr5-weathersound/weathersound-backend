from django.core.management import BaseCommand
from ...models import City, Country
import pathlib


class Command(BaseCommand):

    def handle(self, *args, **options):
        '''
        Cette fonction a pour objectif de charger les villes contenues
        dans le fichier api/files/cities.csv dans la BDD sqlite3.
        '''
        path = str(pathlib.Path(__file__).parent.resolve())
        path = path.replace("/management/commands", "/files/cities.csv")
        with open(path, 'r') as file:
            count = 0
            while True:
                line = file.readline()
                if not line:
                    break
                data = line.split(';')
                if count != 0:
                    country, created = Country.objects.get_or_create(
                        code=data[6])
                    country.name = data[7]
                    country.save()
                    city, created = City.objects.get_or_create(geoname_id=data[0], country=country)
                    city.name = data[1]
                    city.latitude = data[-1].split(',')[0]
                    city.longitude = data[-1].split(',')[1].replace("\n", "")
                    city.inhabitant = 0 if data[13] == "" else data[13]
                    city.save()
                if count % 10000 == 0:
                    print(count)
                count += 1
