#!/bin/bash
cd  /app && \
    python WeatherSound/manage.py collectstatic --no-input && \
    pipenv --clear && \
    rm Pipfile Pipfile.lock && \
cd WeatherSound && \
gunicorn WeatherSound.wsgi --bind 0.0.0.0:8000 
