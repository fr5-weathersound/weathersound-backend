FROM python:3.8-slim

WORKDIR /app

ENV VERSION=1 \
    API_KEY="" \
    ALLOWED_HOSTS="localhost 127.0.0.1 0.0.0.0" \
    DEBUG=0

COPY Pipfile Pipfile.lock entrypoint.sh /app/
COPY WeatherSound ./WeatherSound

RUN pip install --no-cache-dir pipenv==2021.11.23 && \
    pipenv install --system --deploy --ignore-pipfile && \
    chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
